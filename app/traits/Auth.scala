package traits

import play.api.mvc.{Request, Action, Result, Controller}

trait Auth {
  self:Controller =>
  def Secured(f: (String,Request[_]) => Result) = Action { implicit request =>
    val username = request.session.get("username")
    username.fold(Redirect("/login/"))(user=>f(user,request))
  }
}
