package models

import anorm._
import play.api.db.DB
import play.api.Play.current
import scala.xml.Utility.Escapes
import org.apache.commons.lang3.StringEscapeUtils

case class Message(id:Long,userId:Long,title:String,text:String,username:Option[String] = None)

object Message {

  val messageParser = SqlParser.long("id")~SqlParser.long("user_id")~SqlParser.str("title")~SqlParser.str("text") map {
    case id~userId~title~text => Message(id,userId,title,text)
  }

  val messageWithUserParser = messageParser~SqlParser.str("username") map{
    case message~username => message.copy(username = Option(username))
  }

  def getUserMessages(userId:Long,limit:Int):List[Message] = DB.withConnection{
    implicit c =>
      SQL("SELECT * from Messages where user_id={user_id} ORDER BY id")
        .on('user_id->userId,'limit->limit)
        .as(messageParser *)
  }

  def getLastUserMessageId(userId:Long):Option[Long] = DB.withConnection{
    implicit c =>
      SQL("SELECT id from Messages where user_id={user_id}")
        .on('user_id -> userId)
        .as(SqlParser.long("id") singleOpt)
  }

  def getUserMentionedMessages(username:String,limit:Int):List[Message] = DB.withConnection{
    implicit c =>

      SQL(
        """SELECT *,User.username from Messages,User where Messages.user_id=User.id
          |AND Messages.text LIKE CONCAT(CONCAT('%@',{username}),'%') ORDER BY posted_at DESC""".stripMargin)
        .on('username->username,'limit->limit).as(messageWithUserParser *)
  }

  def getPublicMessages(limit:Int):List[Message] = DB.withConnection{
    implicit c =>
      SQL("SELECT *,User.username from Messages,User where Messages.user_id=User.id" +
        " AND text LIKE '%@all %' ORDER BY posted_at DESC").as(messageWithUserParser *)
  }

  def getMessage(id:Long,userId:Long):Option[Message] = DB.withConnection{
    implicit c =>
      SQL("SELECT * from Messages where id={id} AND user_id={user_id}").on(
        'id->id,
        'user_id -> userId
      ).as(messageParser.singleOpt)
  }

  def updateMessage(id:Long,userId:Long,message:Message) = DB.withConnection{
    implicit c =>
      SQL("UPDATE Messages SET text={text} WHERE id={id} AND user_id={user_id}").on(
        'id->id,
        'user_id->userId,
        'text->message.text
      ) executeUpdate()
  }

  def createMessage(message:Message) = DB.withConnection {
    implicit c =>
      SQL("INSERT INTO Messages (id,user_id,title,text,posted_at) VALUES ({id},{user_id},{title},{text},CURRENT_TIMESTAMP)").on(
        'id -> message.id,
        'user_id -> message.userId,
        'title -> message.title,
        'text -> message.text
      ).executeInsert()
  }





}
