package models

import anorm._
import play.api.db.DB
import play.api.Play.current
import org.mindrot.jbcrypt.BCrypt

case class User(id:Long,username:String,password:String)
case class LoginError(message:String)

object User {

  val singleSafeParser = SqlParser.long("id")~SqlParser.str("username") map {
    case id~username => User(id,username,"")
  }

  def getUserById(id:Long):Option[User] = DB.withConnection { implicit c =>
    SQL("SELECT id,username FROM User WHERE id=$id").as(singleSafeParser.singleOpt)
  }

  def getUserByUsername(username:String):Option[User] = DB.withConnection {
    implicit c =>
      SQL("SELECT id,username FROM User WHERE username={username}").on(
        'username ->username
      ).as(singleSafeParser.singleOpt)
  }

  def getAllUsers:List[User] = DB.withConnection {
    implicit c =>
      SQL("SELECT id,username FROM User").as(singleSafeParser *)
  }

  def insertUser(user:User):Option[LoginError] = DB.withConnection {
    implicit c =>

      SQL("INSERT INTO User (username,password) VALUES ({username},{password})")
        .on(
          'username -> user.username,
          'password -> hashPassword(user.password)
        ).executeInsert() match {
          case Some(long) => None
          case None => Option(LoginError("Cannot create user - %s, insert failed".format(user.username)))
          case _ => Option(LoginError("Unexpected error!"))
        }
  }

  def deleteUser(id:Long) = DB.withConnection {
    implicit c =>
      SQL("DELETE FROM User WHERE id={id}").on(
        'id->id
      ).execute()
  }

  def tryCreate(username: String, password: String): Option[LoginError] ={
    //validate Ascii
    if(password.toList.filter(rune => !(255/rune>1)).isEmpty){
      insertUser(User(0,username,password))
    }
    else {
      Option(LoginError("Password contains Non Ascii characters!"))
    }

  }


  def authenticate(username:String, password:String):Option[LoginError] = DB.withConnection {
    implicit c =>
      SQL("SELECT password FROM User WHERE username={username}").on(
        'username -> username
      ).as(SqlParser.str("password").singleOpt).fold(tryCreate(username,password))( hashed => if(BCrypt.checkpw(password,hashed)){None}else{Option(LoginError("Invalid password"))})
  }

  def hashPassword(password:String):String = BCrypt.hashpw(password,BCrypt.gensalt())
}
