package controllers

import play.api.mvc._
import traits.Auth
import models.{Message, User}
import play.api.data.Forms._
import play.api.data._
import play.api.Routes


object Application extends Controller with Auth{

  var messageForm = Form(
    tuple(
      "title" -> nonEmptyText(maxLength = 255),
      "text" -> nonEmptyText(maxLength = 255)
    )
  )

  def index = Secured { (username,request) =>
    Ok(views.html.index(username))
  }

  def postMessage = Secured{ (username,request) =>
    messageForm.bindFromRequest()(request).fold(formWithErrors => BadRequest(views.html.index(username)),messagePair => {
      val u = User.getUserByUsername(username)
      val id = Message.getLastUserMessageId(u.get.id)
      val m = Message(id.getOrElse(0L)+1L,u.get.id,messagePair._1,messagePair._2)
      Message.createMessage(m)
      Redirect("/my/")
    })}

  def myMessages = Secured { (username,request) =>
    val user = User.getUserByUsername(username)
    val messages = Message.getUserMessages(user.get.id,100)
    Ok(views.html.messages(messages,"My Mesages","my",username))
  }

  def publicMessages = Secured { (username,request) =>
    val messages = Message.getPublicMessages(100)
    Ok(views.html.messages(messages,"Public Messages","public",username))
  }

  def mentionedMessages = Secured { (username,request) =>
    val messages = Message.getUserMentionedMessages(username,100)
    Ok(views.html.messages(messages,"Mentioned messages","mention",username))
  }

  def appendToMessage(messageId:Long,username:String,appendText:String) = Secured{ (username,request) =>
      User.getUserByUsername(username).fold(NotFound("No such user"))(user => Message.getMessage(messageId,user.id)
          .fold(NotFound("Message not found"))(message => {
        Message.updateMessage(messageId,
          user.id, Message(message.id, message.userId, message.title, message.text + appendText))
        TemporaryRedirect("/").flashing("success" -> "Message as edited")
    })
    )
  }

}